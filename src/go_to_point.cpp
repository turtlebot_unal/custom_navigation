#include <ros/ros.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>
#include <custom_navigation/NewRefPoint.h>
#include <nav_msgs/GetMap.h>
#include <geometry_msgs/Twist.h>

//Action client
typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;
std::unique_ptr<MoveBaseClient> move_base_ac;

//cmd_vel publisher
std::unique_ptr<ros::Publisher> cmd_vel_pub;

bool getGmappingMap(nav_msgs::GetMapRequest &,
                    nav_msgs::GetMapResponse &)
{
  geometry_msgs::Twist move_cmd;
  move_cmd.angular.z = 3.14159265359; // pi rad/s
  cmd_vel_pub->publish(move_cmd);

  ros::Rate r(10); // 10 hz
  int count = 0;
  while (count < 20)
  {
    move_cmd.angular.z = 3.14159265359; // pi rad/s
    cmd_vel_pub->publish(move_cmd);
    count++;
    r.sleep();
  }
 move_cmd.angular.z = 0;
 cmd_vel_pub->publish(move_cmd);

//TODO return gmapping map

  return true;
}

bool setPointRef(custom_navigation::NewRefPointRequest & req,
                 custom_navigation::NewRefPointResponse & res)
{
  move_base_msgs::MoveBaseGoal goal;

//we'll send a goal to the robot to move 1 meter forward
  goal.target_pose.header.frame_id = "map";
  goal.target_pose.header.stamp = ros::Time::now();

  goal.target_pose.pose.position.x = req.ref_pose.x;
  goal.target_pose.pose.position.y = req.ref_pose.y;
  goal.target_pose.pose.orientation.w = 1.0;

  ROS_INFO("Sending goal");
  move_base_ac->sendGoal(goal);

  ROS_INFO("The new reference point has been defined");
  res.success_msg = "The new reference point has been defined";

  return true;
}

int main(int argc,
         char** argv)
{
  ros::init(argc, argv, "custom_navigation_go_to_point");
  ros::AsyncSpinner spinner(3);
  spinner.start();

  ros::NodeHandle nh;

  cmd_vel_pub.reset(new ros::Publisher);
  *cmd_vel_pub = nh.advertise<geometry_msgs::Twist>("cmd_vel_mux/input/navi", 1, true);

// Create the action client
  move_base_ac.reset(new MoveBaseClient("move_base", true));

//wait for the action server to come up
  while (!move_base_ac->waitForServer(ros::Duration(5.0)))
  {
    ROS_INFO("Waiting for the move_base action server to come up");
  }

  ros::ServiceServer service_1 = nh.advertiseService("set_point_ref", setPointRef);
  ros::ServiceServer service_2 = nh.advertiseService("get_gmapping_map", getGmappingMap);
  ROS_INFO("Ready to set the point reference.");

  ros::waitForShutdown();
  spinner.stop();
  return 0;
}
